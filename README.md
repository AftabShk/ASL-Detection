# ASL Detection

# **Problem Statement:**
Have you ever considered how easy it is to perform simple communication tasks such as ordering food at a drive thru, discussing financial information with a banker, telling a physician your symptoms at a hospital, or even negotiating your wages from your employer?  What if there was a rule where you couldn’t speak and were only able to use your hands for each of these circumstances? The deaf community cannot do what most of the population take for granted and are often placed in degrading situations due to these challenges they face every day. Access to qualified interpretation services isn’t feasible in most cases leaving many in the deaf community with underemployment, social isolation, and public health challenges.

We can understand what the deaf people are talking about, as there each action represents an alphabet and these alphabets can be mapped with words to form a sentence, so that people like us who don't know the sign language will be able to communicate with them.

To give these members of our community a greater voice, I have attempted to answer this question:


**Can computer vision bridge the gap for the deaf and hard of hearing by learning American Sign Language?**

In order to do this, a Yolov4 model was trained on the ASL alphabet.  If successful, it may mark a step in the right direction for both greater accessibility and educational resources.


## Data

Here is the distributions of images: (Letters / Counts)

A - 29  
B - 25  
C - 25  
D - 28  
E - 25  
F - 30  
G - 30  
H - 29  
I - 30  
J - 38  
K - 27  
L - 28  
M - 28  
N - 27  
O - 28  
P - 25  
Q - 26  
R - 25  
S - 30  
T - 25  
U - 25  
V - 28  
W - 27  
X - 26  
Y - 26  
Z - 30  

## Inference 

<p align="center"><img src="./Output/ASL_output.png" width=100%></p><br>

# To-do 
1. Train the dataset on YoloR
2. To Study how we can convert actions into sentence.
